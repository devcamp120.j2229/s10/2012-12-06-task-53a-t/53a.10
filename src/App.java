import models.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        // Subtask 3 + 4
        System.out.println(circle1.toString());
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());

        System.out.println(circle2.toString());
        System.out.println(circle2.getArea());
        System.out.println(circle2.getCircumference());
    }
}
