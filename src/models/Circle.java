package models;

public class Circle {
    //Thuộc tính
    private double radius = 1.0;

    // Phương thức khởi tạo (Công cụ tự sinh)
    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    // Các phương thức get & set (Công cụ tự sinh)
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    // Các hàm khác (viết theo mô tả)
    public double getArea() {
        return Math.PI * this.radius * this.radius;
    }

    public double getCircumference() {
        return 2 * Math.PI * this.radius;
    }

    public String toString() {
        return "Circle[radius=" + this.radius + "]";
    }
}
